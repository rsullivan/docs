The application code
=====================

This is a flask application

models.py
---------

.. automodule:: app.models
	:members:
	:undoc-members:

views.py
--------

.. automodule:: app.main.views
	:members:
	:undoc-members:


forms.py
--------

.. automodule:: app.main.forms.py
	:members:
	:undoc-members:


errors.py
---------

.. automodule:: app.main.errors
	:members:
	:undoc-members:
