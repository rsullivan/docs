Unit and functional tests
=========================

tests\\base.py
---------------

.. automodule :: tests.base
	:members:
	:undoc-members:


tests\\test_basic_classifier.py
--------------------------------

.. automodule :: tests.test_basic_classifier
	:members:
	:undoc-members:
	

tests\\test_citation_groups.py
-------------------------------

.. automodule :: tests.test_citation_groups
	:members:
	:undoc-members:


tests\\test_models.py
----------------------

.. automodule :: tests.test_models
	:members:
	:undoc-members:


tests\\test_preprocessing_data.py
----------------------------------

.. automodule :: tests.test_preprocessing_data
	:members:
	:undoc-members:


tests\\test_views.py
---------------------
.. automodule :: tests.test_views
	:members:
	:undoc-members:

tests\\test_webtest_bokeh.py
-----------------------------

.. automodule :: tests.test_webtest_bokeh
	:members:
	:undoc-members:


tests\\test_webtest_tests.py
-----------------------------
.. automodule :: tests.test_webtest_tests
	:members:
	:undoc-members:


