The classification process code
================================

base.py
-------

.. automodule:: predictocite.datasets.base
   :members:
   :undoc-members:

   
   
citation_groups.py
-------------------

.. automodule:: predictocite.datasets.citation_groups
   :members:
   :undoc-members:



   
main.py
----------------

.. automodule:: predictocite.main
   :members:
   :undoc-members: